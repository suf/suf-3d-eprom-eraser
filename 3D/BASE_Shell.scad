$fn=180;


module shell(swidth,sdepth,sheight)
{
    corner_dia = 10.001;
    wall_thickness = 2.5;
    color("gray")
    {
        difference()
        {
            hull()
            {
                translate([swidth/-2,0,sheight])
                    cube([swidth,sdepth,0.001]);
                for(i=[-2,2])
                    translate([(swidth-corner_dia)/i,0,corner_dia/2])
                        rotate([-90,0,0])
                            cylinder(d=corner_dia,h=sdepth);
            }
            hull()
            {
                translate([swidth/-2 + wall_thickness,-0.001,sheight+0.001])
                    cube([swidth-2*wall_thickness,sdepth + 0.002,0.001]);
                for(i=[-2,2])
                    translate([(swidth-corner_dia)/i,-0.001,corner_dia/2])
                        rotate([-90,0,0])
                            cylinder(d=corner_dia-2*wall_thickness,h=sdepth+0.002);
            }
        }
        for(i=[sdepth-wall_thickness, sdepth-wall_thickness*3-0.3])
        {
            translate([0,i,0])
            {
                difference()
                {
                    hull()
                    {
                        translate([swidth/-2 + wall_thickness,0,sheight])
                            cube([swidth-2*wall_thickness,wall_thickness,0.001]);
                        for(i=[-2,2])
                            translate([(swidth-corner_dia)/i,-0.001,corner_dia/2])
                                rotate([-90,0,0])
                                    cylinder(d=corner_dia-2*wall_thickness,h=wall_thickness);
                    }
                    hull()
                    {
                        translate([swidth/-2 + 2* wall_thickness,-0.002,sheight+0.001])
                            cube([swidth-4*wall_thickness,wall_thickness+0.004,0.001]);
                        for(i=[-2,2])
                            translate([(swidth-corner_dia)/i,-0.002,corner_dia/2])
                                rotate([-90,0,0])
                                    cylinder(d=corner_dia-4*wall_thickness,h=wall_thickness+0.004);
                    }
                }
            }
        }
    }
}

shell(100,200,50);