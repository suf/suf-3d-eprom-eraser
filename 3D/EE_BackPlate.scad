$fn=180;

module back_plate(pheight,pwidth)
{
    corner_dia = 5;
    wall_thickness = 2.5;
    minkowski()
    {
        cube([pheight-wall_thickness*2,pwidth-wall_thickness*2,wall_thickness/2],center = true);
        cylinder(d=corner_dia, h= wall_thickness/2);
    }
}

module EE_BackPlate()
{
    back_plate(95,95);
}

EE_BackPlate();