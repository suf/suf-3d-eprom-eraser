$fn=180;

module EE_Timer()
{
    minkowski()
    {
        cube([56,38,0.001],center=true);
        cylinder(d=4,h=31);
    }
    translate([0,0,31])
    {
        minkowski()
        {
            cube([59,37,0.001],center=true);
            cylinder(d=8,h=3);
        }
    }

    translate([30,0,20])
    {
        rotate([0,15,0])
        {
            translate([-1,-5,0])
            {
                cube([1,10,10]);
            }
        }
    }

    translate([-30,0,20])
    {
        rotate([0,-15,0])
        {
            translate([0,-5,0])
            {
                cube([1,10,10]);
            }
        }
    }
}

EE_Timer();