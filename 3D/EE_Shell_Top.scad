$fn=180;

use <BASE_Shell.scad>

module EE_Shell_Top()
{
    difference()
    {
        shell(100,200,50);
            for(i=[0,1])
                for(j=[9.75,169.75])
                    translate([-50.001+i*100.002,j,39.5])
                        mirror([i,0,0])
                        {
                            cube([2.502,10.5,10.502]);
                            translate([-0.001,15.25,5.5])
                                rotate([0,90,0])
                                {
                                    cylinder(d1=6,d2=0.001,h=3);
                                    cylinder(d=3.5,h=2.504);
                                }
                        }
    }
}
EE_Shell_Top();
