$fn=180;

use <EE_Shell_Bottom.scad>
use <EE_Shell_Top.scad>
use <EE_MidPlate.scad>
use <EE_BackPlate.scad>
use <EE_Tray.scad>
use <EE_Timer.scad>
use <components/furniture_rail_15x10.scad>

tray_open=200;

translate([0,0,45])
{
    EE_MidPlate();
    translate([-42,70,4+2.5])
        psu();
}

EE_Shell_Bottom();

for(i=[-1,1])
    translate([-35*i,3,2.5])
        rail(tray_open*-1);

translate([0,tray_open*-1,12.5])
    EE_Tray();

/*
translate([0,0,100])
    rotate([0,180,0])
        EE_Shell_Top();
*/
/*
translate([0,196.75,50])
    rotate([90,0,0])
        EE_BackPlate();
*/

module psu()
{
    color("red")
    {
        difference()
        {
            cube([23.8,51,1.6]);
            for(i=[3.1,20.5])
                for(j=[2.9,48.1])
                    translate([i,j,-0.001])
                        cylinder(d=4,h=1.602);
        }
    }
}

translate([0,28.5, 72])
rotate([90,0,0])
{
    EE_Timer();
}