$fn=180;

module EE_MidPlate()
{
    difference()
    {
        union()
        {
            // middle plate
            translate([-47.5,0,0])
                cube([95,212,2.5]);
            // PSU spacer
            translate([-42,70,0])
                for(i=[3.1,20.5])
                    for(j=[2.9,48.1])
                        translate([i,j,0])
                            cylinder(d=7,h=6.5);
            // UV protector
            translate([-15,202-20-130,-0.001])
                cube([2.5,130,30]);
            translate([12.5,202-20-130,-0.001])
                cube([2.5,130,30]);
        }
        // tube mount
        translate([0,202-20,-0.001])
        {
            cylinder(d=3.5,h=2.502);
        }
        translate([0,202-20-127,-0.001])
        {
            cylinder(d=3.5,h=2.502);
        }
        // tube cutout
        translate([-12.5,202-20-127+8.5,-0.001])
            cube([25,110,2.502]);
        // electronic balast mount
        translate([30,192-10,-0.001])
        {
            cylinder(d=3.5,h=2.502);
        }
        translate([30,192-10-150,-0.001])
        {
            cylinder(d=3.5,h=2.502);
        }
        // PSU mount
        translate([-42,70,0])
            for(i=[3.1,20.5])
                for(j=[2.9,48.1])
                    translate([i,j,-0.001])
                        cylinder(d=3.5,h=7.002);
    }
    // Side mount
    for(i=[-1,1])
        for(j=[20,180])
            translate([i*43.5,j,7.5])
                difference()
                {
                    cube([8,20,15],center=true);
                    for(k=[-5,5])
                    {
                        translate([-4.001,k,2.5])
                            rotate([0,90,0])
                                cylinder(d=3.5,h=8.002);
                        translate([0,k,3])
                        {
                            cube([2.7,5.7,10],center=true);
                        }
                    }
                    
                }
}
EE_MidPlate();

