$fn=180;

module EE_Tray()
{
    difference()
    {
        translate([-45,0,0])
            cube([90,180,10]);
        for(i=[-1,1])
            for(j=[19+3,182+3-19])
                translate([35*i,j,-0.001])
                    cylinder(d=4.5,h=2.502);
        translate([-42.5,-0.001,2.5])
            cube([85,177.5,7.501]);
    }
}

EE_Tray();